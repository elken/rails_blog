@Post = React.createClass

  render: ->
    React.DOM.div
      className: "post"
      React.createElement PostHeader, post: @props.post
      React.createElement PostContent, post: @props.post

@PostHeader = React.createClass

  render: ->
    React.DOM.div
      className: "post-header"
      React.DOM.h2
        className: "post-title"
        @props.post.title
      React.DOM.div
        className: "post-meta"
        "By #{ @props.post.author } - #{ @props.post.created_at }"

@PostContent = React.createClass

  render: ->
    React.DOM.div
      className: "post-contents"
      @props.post.contents