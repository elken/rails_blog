@PostsList = React.createClass

  getInitialState: ->
    posts: @props.initialPosts

  render: ->
    posts = @state.posts.map (post) ->
      React.createElement Post, key: post.id, post: post

    React.DOM.div
      className: "posts"
      posts